package co.jaimlus.consumewsdl.client;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import co.jaimlus.test.Add;
import co.jaimlus.test.AddResponse;

public class CalculatorClient extends WebServiceGatewaySupport {

	public int add(int numberOne, int numberTwo) {
		int result;

		Add request = new Add();
		request.setIntA(numberOne);
		request.setIntB(numberTwo);

		AddResponse response = (AddResponse) getWebServiceTemplate().marshalSendAndReceive(
				"http://www.dneonline.com/calculator.asmx?WSDL", request,
				new SoapActionCallback("http://tempuri.org/Add"));

		result = response.getAddResult();

		return result;
	}

}
