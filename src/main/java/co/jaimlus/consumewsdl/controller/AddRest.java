package co.jaimlus.consumewsdl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import co.jaimlus.consumewsdl.client.CalculatorClient;

@RestController
public class AddRest {

	@Autowired
	private CalculatorClient calculatorClient;
	
	@GetMapping("/add-numbers/one/{numberOne}/two/{numberTwo}")
	public int add(@PathVariable int numberOne, @PathVariable int numberTwo) {
		int result = calculatorClient.add(numberOne, numberTwo);
		return result;
	}
}
